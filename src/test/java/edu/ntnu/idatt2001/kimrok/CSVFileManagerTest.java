package edu.ntnu.idatt2001.kimrok;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.*;

import static org.junit.jupiter.api.Assertions.*;

class CSVFileManagerTest {

    @Test
    @DisplayName("Import Patient From CSV")
    void importPatientFromCSV() throws IOException {
        // Creates a temporary file
        File file = File.createTempFile("test", ".csv");
        file.deleteOnExit();

        // Adds data to the file
        try (FileWriter fileWriter = new FileWriter(file)) {
            fileWriter.append("firstName;lastName;generalPractitioner;socialSecurityNumber;\n");
            fileWriter.append("firstName1;lastName1;doctor1;SSN1;\n");
            fileWriter.append("firstName2;lastName2;doctor2;SSN2;\n");
            fileWriter.append("firstName3;lastName3;doctor3;SSN3;\n");
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Sets path and imports from file
        CSVFileManager.getINSTANCE().setPath(file.toString());
        CSVFileManager.getINSTANCE().importPatientFromCSV();

        // The second Patient (first line is not a Patient) added should be Patient number 2 with the index 1
        assertEquals("lastName2", PatientRegister.getPatients().get(1).getLastName());
    }

    @Test
    @DisplayName("Export Patient To CSV")
    void exportPatientToCSV() throws IOException {
        // Creates a temporary file
        File file = File.createTempFile("test", ".csv");
        file.deleteOnExit();

        // Adds data to register
        PatientRegister.addPatient(new Patient("firstName1", "lastName1", "SSN1", "diagnose1", "doctor1"));
        PatientRegister.addPatient(new Patient("firstName2", "lastName2", "SSN2", "diagnose2", "doctor2"));
        PatientRegister.addPatient(new Patient("firstName3", "lastName3", "SSN3", "diagnose3", "doctor3"));

        // Sets path and exports to file
        CSVFileManager.getINSTANCE().setPath(file.toString());
        CSVFileManager.getINSTANCE().exportPatientToCSV();

        // reads third line and checks if it was stored correctly
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(file))) {
            bufferedReader.readLine(); // Will skip first line (not a Patient)
            bufferedReader.readLine(); // Will skip first Patient
            bufferedReader.readLine(); // Will skip second Patient
            String line = bufferedReader.readLine();
            assertEquals("firstName3;lastName3;doctor3;SSN3;", line);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}