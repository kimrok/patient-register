package edu.ntnu.idatt2001.kimrok;

import javafx.scene.control.ToolBar;

/**
 * Class for creating JavaFX ToolBar
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
public class FXToolBar extends Node {

    @Override
    void create() {
        toolBar = new ToolBar();
    }
}
