package edu.ntnu.idatt2001.kimrok;

import java.util.ArrayList;

/**
 * Register for the patients
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.04.27
 * @since 2021.04.27
 */
public class PatientRegister {
    private static ArrayList<Patient> patients = new ArrayList<>();

    /**
     * No parameters in constructor. Do not create an instance of this class.
     */
    private PatientRegister() {
        throw new IllegalStateException("Unable to create an instance of " + getClass().getSimpleName());
    }

    /**
     * Adds the patient
     *
     * @param patient
     */
    public static void addPatient(Patient patient) {
        patients.add(patient);
    }

    /**
     * Removes the patient
     *
     * @param patient
     */
    public static void removePatient(Patient patient) {
        patients.remove(patient);
    }

    /**
     * @return ArrayList with all patients
     */
    public static ArrayList<Patient> getPatients() {
        return patients;
    }
}
