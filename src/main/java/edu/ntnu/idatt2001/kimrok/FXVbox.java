package edu.ntnu.idatt2001.kimrok;

import javafx.scene.layout.VBox;

/**
 * Class for creating JavaFX VBox
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
public class FXVbox extends Node{
    @Override
    void create() {
        vBox = new VBox();
    }
}
