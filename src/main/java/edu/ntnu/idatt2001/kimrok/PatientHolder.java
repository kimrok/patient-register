package edu.ntnu.idatt2001.kimrok;

/**
 * Singleton class for holding on to a Patient
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.05
 * @since 2021.05.05
 */
public final class PatientHolder {
    private Patient patient;
    private final static PatientHolder INSTANCE = new PatientHolder();

    /**
     * No parameters in constructor. Do not create an instance of this class.
     */
    private PatientHolder() {}

    /**
     * @return INSTANCE
     */
    public static PatientHolder getInstance() {
        return INSTANCE;
    }

    /**
     * @return Patient
     */
    public Patient getPatient() {
        return this.patient;
    }

    /**
     * @param patient
     */
    public void setPatient(Patient patient) {
        this.patient = patient;
    }
}
