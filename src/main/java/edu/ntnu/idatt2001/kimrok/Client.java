package edu.ntnu.idatt2001.kimrok;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Client for the patient register
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.04.27
 * @since 2021.04.27
 */
public class Client extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("/PatientRegister.fxml"));
        primaryStage.setTitle("Patient Register");
        primaryStage.setResizable(false);
        primaryStage.setScene(new Scene(root));
        primaryStage.show();
    }
}
