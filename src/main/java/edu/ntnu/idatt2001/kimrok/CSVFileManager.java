package edu.ntnu.idatt2001.kimrok;

import java.io.*;

/**
 * Singleton class for file management
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.05
 * @since 2021.05.05
 */
public final class CSVFileManager {
    private String path;
    private final static CSVFileManager INSTANCE = new CSVFileManager();

    /**
     * No parameters in constructor. Do not create an instance of this class.
     */
    private CSVFileManager() {}

    /**
     * @return INSTANCE
     */
    public static CSVFileManager getINSTANCE() {
        return INSTANCE;
    }

    /**
     * @return path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * Reads a .csv file stored in the path directory
     * Uses semicolon (;) as separators
     * Creates Patients with the following info firstName;lastName;generalPractitioner;socialSecurityNumber
     */
    public void importPatientFromCSV() {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(path))) {
            bufferedReader.readLine(); // Will skip first line
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                String[] patientInfo = line.split(";");
                String firstName = patientInfo[0];
                String lastName = patientInfo[1];
                String generalPractitioner = patientInfo[2];
                String socialSecurityNumber = patientInfo[3];
                Patient patient = new Patient(firstName, lastName, socialSecurityNumber, "", generalPractitioner);
                PatientRegister.addPatient(patient);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    /**
     * saves a .csv file in the path directory
     * Uses semicolon (;) as separators
     * Stores Patients with the following info firstName;lastName;generalPractitioner;socialSecurityNumber
     */
    public void exportPatientToCSV() {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(path))) {
            bufferedWriter.write("firstName;lastName;generalPractitioner;socialSecurityNumber\n");
            for (Patient patient:PatientRegister.getPatients()) {
                bufferedWriter.write(
                        patient.getFirstName() + ";" +
                        patient.getLastName() + ";" +
                        patient.getGeneralPractitioner() + ";" +
                        patient.getSocialSecurityNumber() + ";\n");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
