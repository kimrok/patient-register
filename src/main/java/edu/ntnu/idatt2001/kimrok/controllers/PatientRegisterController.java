package edu.ntnu.idatt2001.kimrok.controllers;

import edu.ntnu.idatt2001.kimrok.*;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.FileChooser;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.File;
import java.io.IOException;

/**
 * Controller for the patient register
 *
 * @author Kim Johnstuen Rokling
 * @version 1.2.0 2021.05.0
 * @since 2021.04.27
 */
public class PatientRegisterController {
    @FXML
    public TableColumn<Patient, String> columnFirstName, columnLastName, columnSocialSecurityNumber;
    public TableView<Patient> tableView;
    private ObservableList<Patient> patientList = FXCollections.observableArrayList();

    public void initialize() {
        updatePatients();
    }
    private void updatePatients() {
        patientList.clear();
        patientList.addAll(PatientRegister.getPatients());
        columnFirstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        columnLastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        columnSocialSecurityNumber.setCellValueFactory(new PropertyValueFactory<>("socialSecurityNumber"));
        tableView.setItems(patientList);
    }

    /**
     * A centralized mathod for loading other stages
     *
     * @param event
     * @param screen
     * @param title
     * @param showAndWait
     * @throws IOException
     */
    private void stageLoader(ActionEvent event, String screen, String title, boolean showAndWait) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource(screen));
        Parent root = fxmlLoader.load();
        Stage stage = new Stage();
        stage.setTitle(title);
        stage.setResizable(false);
        stage.initModality(Modality.APPLICATION_MODAL); // Prevents interaction with other windows until closed
        stage.setScene(new Scene(root));
        if (showAndWait) {
            stage.showAndWait();
        } else {
            stage.show();

        }
    }

    /**
     * Pops open a smaller windows where the user can add patient information
     *
     * @param event
     * @throws IOException
     */
    public void addNewPatient(ActionEvent event) throws IOException {
        stageLoader(event, "/AddPatient.fxml", "Patient Details - Add", true);
        //stageUpdate(event);
        updatePatients();
    }

    /**
     * Pops open a smaller windows where the user can edit patient information
     *
     * @param event
     * @throws IOException
     */
    public void editPatient(ActionEvent event) throws IOException {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            PatientHolder.getInstance().setPatient(tableView.getSelectionModel().getSelectedItem());
            stageLoader(event, "/AddPatient.fxml", "Patient Details - Edit", true);
            updatePatients();
        }
    }

    /**
     * Pops open a smaller windows where the user can remove a patient
     *
     * @param event
     * @throws IOException
     */
    public void removePatient(ActionEvent event) throws IOException {
        if (tableView.getSelectionModel().getSelectedItem() != null) {
            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Are you sure you want to delete this item?", ButtonType.YES, ButtonType.NO);
            alert.setTitle("Delete confirmation");
            alert.setHeaderText("Delete confirmation");
            alert.setResizable(false);
            alert.showAndWait();
            if (alert.getResult() == ButtonType.YES) {
                PatientRegister.removePatient(tableView.getSelectionModel().getSelectedItem());
                updatePatients();
            }
        }

    }

    /**
     * Allows the user to import patients from a CSV file
     *
     * @param event
     */
    public void importCSV(ActionEvent event) {
        // Opens file browser and stores result in "file"
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Import .csv file");
        File file = fileChooser.showOpenDialog(new Stage());

        if (file != null) {
            // Gets the last 4 characters of the file
            String fileExtention = file.toString().substring(file.toString().length() - 4);

            if (fileExtention.equals(".csv")) {
                // Sets the path for the file, imports from it and updates the table
                CSVFileManager.getINSTANCE().setPath(file.toString());
                CSVFileManager.getINSTANCE().importPatientFromCSV();
                updatePatients();

            } else {
                // If file is not .csv then will give warning
                ButtonType selectFile = new ButtonType("Select File");
                Alert alert = new Alert(Alert.AlertType.ERROR,
                        "Please choose another file by clicking on Select File or cancel the operation",
                        selectFile, ButtonType.CANCEL);
                alert.setTitle("Error loading file");
                alert.setHeaderText("The chosen file type is not valid.");
                alert.setResizable(false);
                alert.showAndWait();

                // The option to run this entire method again
                if (alert.getResult() == selectFile) {
                    importCSV(event);
                }
            }
        }
    }

    /**
     * Allows the user to export patients to a CSV file
     *
     * @param event
     */
    public void exportCSV(ActionEvent event) {
        // Opens file browser and stores result in "file"
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Export .csv file");
        File file = fileChooser.showSaveDialog(new Stage());

        if (file != null) {
            // Gets the last 4 characters of the file
            String fileExtention = file.toString().substring(file.toString().length() - 4);

            if (fileExtention.equals(".csv")) {
                // Sets the path for the file and exports to it
                CSVFileManager.getINSTANCE().setPath(file.toString());
                CSVFileManager.getINSTANCE().exportPatientToCSV();

            } else {
                // If file is not .csv then will give warning
                ButtonType tryAgain = new ButtonType("Try Again");
                Alert alert = new Alert(Alert.AlertType.ERROR,
                        "please export as a .csv file",
                        tryAgain, ButtonType.CANCEL);
                alert.setTitle("Error exporting file");
                alert.setHeaderText("The chosen file type is not valid.");
                alert.setResizable(false);
                alert.showAndWait();

                // The option to run this entire method again
                if (alert.getResult() == tryAgain) {
                    exportCSV(event);
                }
            }
        }
    }

    /**
     * Exits the program
     *
     * @param event
     */
    public void exit(ActionEvent event) {
        Platform.exit();
    }

    /**
     * Shows information about the program
     *
     * @param event
     */
    public void about(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.INFORMATION, "A brilliant application created by\n(C) Kim Rokling\n2021-04-30", ButtonType.OK);
        alert.setTitle("Information Dialog - About");
        alert.setHeaderText("Patients Register\nVersion 1.0.0 STABLE\nWe've been trying to reach you about your cars extended warranty!");
        alert.setResizable(false);
        alert.show();
    }
}
