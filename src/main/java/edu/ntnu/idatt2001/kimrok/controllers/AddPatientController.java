package edu.ntnu.idatt2001.kimrok.controllers;

import edu.ntnu.idatt2001.kimrok.Patient;
import edu.ntnu.idatt2001.kimrok.PatientHolder;
import edu.ntnu.idatt2001.kimrok.PatientRegister;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.TextField;

/**
 * Controller for adding and editing a patient to the register
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.04.27
 * @since 2021.04.27
 */
public class AddPatientController {
    @FXML
    public TextField firstName, lastName, socialSecurityNumber;

    public void initialize() {
        // There are a few if statements because of the assignment requirement that add and edit is the same window.

        // For editing a Patient
        if (PatientHolder.getInstance().getPatient() != null) {
            Patient patient = PatientHolder.getInstance().getPatient();
            firstName.setText(patient.getFirstName());
            lastName.setText(patient.getLastName());
            socialSecurityNumber.setText(patient.getSocialSecurityNumber());
        }
    }

    /**
     * Changes or adds a patient with the information provided
     *
     * @param event
     */
    public void ok(ActionEvent event) {
        // There are a few if statements because of the assignment requirement that add and edit is the same window.

        // No fields can be blank
        if (firstName.getText().isBlank() || lastName.getText().isBlank() || socialSecurityNumber.getText().isBlank()) {
            Alert alert = new Alert(Alert.AlertType.ERROR,
                    "Please fill out all the information of the patient",
                    ButtonType.OK);
            alert.setTitle("Error in data provided");
            alert.setHeaderText("Error in data provided");
            alert.setResizable(false);
            alert.show();

            // This is for editing a Patient
        } else if (PatientHolder.getInstance().getPatient() != null) {
            Patient patient = PatientHolder.getInstance().getPatient();
            patient.setFirstName(firstName.getText());
            patient.setLastName(lastName.getText());
            patient.setSocialSecurityNumber(socialSecurityNumber.getText());
            PatientHolder.getInstance().setPatient(null);

            // This is for adding a patient
        } else {
            PatientRegister.addPatient(new Patient(firstName.getText(), lastName.getText(), socialSecurityNumber.getText(), "", ""));
        }
        close(event);
    }

    /**
     * Exits the window without saving
     *
     * @param event
     */
    public void cancel(ActionEvent event) {
        close(event);
    }

    /**
     * Closes the window
     *
     * @param event
     */
    private void close(ActionEvent event) {
        PatientHolder.getInstance().setPatient(null);
        ((Node) event.getSource()).getScene().getWindow().hide();
    }
}
