package edu.ntnu.idatt2001.kimrok;

import javafx.scene.control.TableView;

/**
 * Class for creating JavaFX TableView
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
class FXTableView extends Node{
    @Override
    public void create() {
        tableView = new TableView();
    }
}
