package edu.ntnu.idatt2001.kimrok;

import javafx.scene.control.MenuBar;

/**
 * Class for creating JavaFX MenuBar
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
class FXMenuBar extends Node {

    @Override
    public void create() {
        menu = new MenuBar();
    }
}
