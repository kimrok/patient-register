package edu.ntnu.idatt2001.kimrok;

import javafx.scene.control.MenuBar;
import javafx.scene.control.TableView;
import javafx.scene.control.ToolBar;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;

/**
 * Abstract node class for JavaFX nodes
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
abstract class Node {
    protected MenuBar menu;
    protected TableView tableView;
    protected ToolBar toolBar;
    protected BorderPane borderPane;
    protected VBox vBox;

    abstract void create();
}
