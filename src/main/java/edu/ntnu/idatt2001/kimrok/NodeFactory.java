package edu.ntnu.idatt2001.kimrok;

/**
 * Node Factory for creating JavaFX nodes
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
public class NodeFactory {

    public Node getNode(String nodeType) {

        if (nodeType.equalsIgnoreCase("menubar")) {
            return new FXMenuBar();
        } else if (nodeType.equalsIgnoreCase("toolbar")) {
            return new FXToolBar();
        } else if (nodeType.equalsIgnoreCase("borderpane")) {
            return new FXBorderPane();
        } else if (nodeType.equalsIgnoreCase("vbox")) {
            return new FXVbox();
        } else if (nodeType.equalsIgnoreCase("tableview")) {
            return new FXTableView();
        }

        return null;
    }
}
