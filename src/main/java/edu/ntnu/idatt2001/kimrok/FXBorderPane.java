package edu.ntnu.idatt2001.kimrok;

import javafx.scene.layout.BorderPane;

/**
 * Class for creating JavaFX BorderPane
 * For demonstrative purposes only, do not use.
 *
 * @author Kim Johnstuen Rokling
 * @version 1.0.0 2021.05.04
 * @since 2021.05.04
 */
public class FXBorderPane extends Node{
    @Override
    void create() {
        borderPane = new BorderPane();
    }
}
