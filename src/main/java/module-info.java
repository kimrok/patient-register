module idatt.javafx {
    requires javafx.controls;
    requires javafx.graphics;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.kimrok.controllers to javafx.fxml;

    exports edu.ntnu.idatt2001.kimrok;
}